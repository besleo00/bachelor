import math
from Datenstruktur.Node import Node
from Datenstruktur.Edge import Edge

def construct_graph(graph, co_file_location, gr_file_location):
    """
    input: Ein leerer Graph graph, Ort der co_file und Ort der gr_file
    output: Baut anhand der co- und gr-Datei den Graphen graph auf.
    """
    nodes = open(co_file_location)
    arcs = open(gr_file_location)
    
    arc_content = arcs.read()
    nodes_content = nodes.read()
    nodes.close()
    arcs.close()
    node_saver = {}
    node_list = []
    edge_list = []
    index = []
    edge_index = []
    arc_content = arc_content.splitlines()
    nodes_content = nodes_content.splitlines()
    for l in nodes_content:
        s_l =  l.split(' ')
        t = (int(s_l[2]),int(s_l[3]))
        node = Node(t[0],t[1])
        index.append(t) 
        node_list.append(node)
        node_saver[s_l[1]] = node
    
    counter = 0
    for l in arc_content:
        s_l =  l.split(' ')
        if s_l[1] in node_saver:
            t = (node_saver[s_l[1]],node_saver[s_l[2]])
            x1, y1 = t[0].get_coords()
            x2, y2 = t[1].get_coords()
            length = round((math.sqrt(math.pow(x1-x2,2)+math.pow(y1-y2,2))))
            edge = Edge(t[0],t[1],length)
            graph.edge_lengths[t] = length
            t[0].adjacent.append(t[1])
            t[1].incoming.append(t[0])

            edge_index.append((t[0].get_coords(),t[1].get_coords())) ##
            counter += 1
            edge_list.append(edge)

    graph.build_graph(node_list, edge_list)