from PriorityQueue.MinHeap import MinHeap
import math
from operator import itemgetter

def visit(dict, node):
    saver = node
    while saver.get_up() != None:
        dict[saver.get_up()] = 1
        saver = saver.get_up()
    dict[node] = 1 
    saver = node
    while saver.get_down() != None:
        dict[saver.get_down()] = 1
        saver = saver.get_down()

def initialize(
    hh,
    start_node,
    end_node,
    front_dist,
    back_dist,
    front_queue,
    back_queue,
    forward_gap,
    backward_gap,
    forward_level,
    backward_level,
    h,
):
    infinity = float("inf")
    for graph in hh:
        for v in graph.get_vertices():
            front_dist[v] = infinity
            back_dist[v] = infinity
    front_dist[start_node] = 0
    back_dist[end_node] = 0
    front_queue.insert(start_node, 0)
    back_queue.insert(end_node, 0)
    forward_gap[start_node] = hh[0].get_f_nachbarschaft(start_node)
    backward_gap[end_node] = hh[0].get_b_nachbarschaft(end_node)
    forward_level[start_node] = 0
    backward_level[end_node] = 0


def front_update_distances(u, v, front_dist, edge_length, front_queue, forward_gap, forward_level, gap, level, pot, end_node):
    new_dist = front_dist[u] + edge_length
    forward_gap[v] = gap - edge_length
    forward_level[v] = level
    if new_dist < front_dist[v]:
        front_dist[v] = new_dist
        front_queue.update(v, new_dist + get_pot(get_init(v),end_node,pot))

def back_update_distances(u, v, back_dist, edge_length, back_queue, backward_gap, backward_level, gap, level, pot, start_node):
    new_dist = back_dist[u] + edge_length
    backward_gap[v] = gap - edge_length
    backward_level[v] = level
    if new_dist < back_dist[v]:
        back_dist[v] = new_dist
        back_queue.update(v, new_dist + get_pot(start_node, get_init(v), pot))

def get_pot(node, end_node, pot):
    out = pot[0]
    inc = pot[1]
    return max(inc[node]-inc[end_node],out[end_node]-out[node])

def get_init(node):
    saver = node
    while saver.get_lvl() != 0:
        saver = saver.get_down()
    return saver

def get_forward_pot(node, end_node ,pot):
    result_list = []
    for out, inc in pot:
        result_list.append(((out,inc),max(inc[node]-inc[end_node],out[end_node]-out[node])))
    out, inc = max(result_list, key = itemgetter(1))[0]
    return (out,inc)

def highway_query_algorithm(hh, start_node, end_node, h, pot):

    if start_node == end_node:
        return 0

    t_length = float("inf")
    forward_dist = {}
    backward_dist = {}
    forward_visited = {}
    backward_visited = {}
    forward_queue = MinHeap()
    backward_queue = MinHeap()
    forward_gap = {}
    backward_gap = {}
    forward_level = {}
    backward_level = {}
    forward_core = set()
    backward_core = set()
    already_forward = {}
    already_backward = {}
    f_flag = True
    b_flag = True

    initialize(
        hh,
        start_node,
        end_node,
        forward_dist,
        backward_dist,
        forward_queue,
        backward_queue,
        forward_gap,
        backward_gap,
        forward_level,
        backward_level,
        h,
    )
    forward_pot = get_forward_pot(start_node, end_node, pot)
    backward_pot = get_forward_pot(end_node, start_node, pot)
    while(((forward_queue.isEmpty() == False) and (f_flag)) or ((backward_queue.isEmpty() == False) and (b_flag))):
        if(forward_queue.isEmpty() == False) and (f_flag):
            u = forward_queue.getMin()
            if u in backward_visited:
                dist = forward_dist[u] + backward_dist[u]
                if t_length > dist:
                    t_length = dist
            visit(forward_visited,u)

            forward_dist_u = forward_dist[u]

            if forward_gap[u] != math.inf:
                f_gap = forward_gap[u]
            else:
                if u.get_up() != None:
                    f_gap = hh[u.get_up().get_lvl()].get_f_nachbarschaft(u.get_up())
                    node = u.get_up()
                    forward_queue.update(node,forward_dist[u])
                    already_forward[u] = 1
                    forward_gap[node] = f_gap
                    forward_level[node] = node.get_lvl()
                    forward_dist[node] = forward_dist[u]
                    continue
                else:
                    f_gap = math.inf


            if (f_gap != math.inf) and (u.get_lvl() == (len(hh)-1)):
                forward_core.add(u)
                for v in backward_core:
                    new_dist = (forward_dist[u] + hh[-1].d_l[(u,v)] + backward_dist[v])
                    if t_length > new_dist:
                        t_length = new_dist
                continue
            
            if forward_dist[u] + get_pot(get_init(u), end_node, forward_pot) > t_length:
                f_flag = False
                continue

            for x in u.get_adjacent_nodes():
                level = forward_level[u]
                l = hh[level].get_edge_length(u, x)
                gap = f_gap

                saver_u = u
                while (l > gap) and (level < (len(hh)-1)):
                    level += 1
                    gap = hh[level].get_f_nachbarschaft(saver_u.get_up())
                    saver_u = saver_u.get_up()
                
                
                if (level > forward_level[u]) and (already_forward.get(u) == None) and (gap != math.inf):
                    node = u.get_up()
                    forward_queue.update(node,forward_dist[u])
                    already_forward[u] = 1
                    forward_gap[node] = gap
                    forward_level[node] = level
                    forward_dist[node] = forward_dist[u]
                    continue

                if (gap != math.inf) and (level == (len(hh)-1)) and (level > forward_level[u]):
                    forward_dist[saver_u] = forward_dist[u]
                    forward_core.add(saver_u)
                    for v in backward_core:
                        new_dist = (forward_dist[saver_u] + hh[-1].d_l[(saver_u,v)] + backward_dist[v])
                        if t_length > new_dist:
                            t_length = new_dist
                    continue

                if x not in forward_visited and (forward_dist[x] > forward_dist_u + l):
                    front_update_distances(
                        u,
                        x,
                        forward_dist,
                        l,
                        forward_queue,
                        forward_gap,
                        forward_level,
                        gap,
                        x.get_lvl(),
                        forward_pot,
                        end_node,
                    )

        if(backward_queue.isEmpty() == False) and (b_flag):
            v = backward_queue.getMin()
            if v in forward_visited:
                dist = forward_dist[v] + backward_dist[v]
                if t_length > dist:
                    t_length = dist
            visit(backward_visited, v)

            backward_dist_v = backward_dist[v]

            if backward_gap[v] != math.inf:
                b_gap = backward_gap[v]
            else:
                if v.get_up() != None:
                    b_gap = hh[v.get_up().get_lvl()].get_b_nachbarschaft(v.get_up())
                    node = v.get_up()
                    backward_queue.update(node,backward_dist[v])
                    already_backward[v] = 1
                    backward_gap[node] = b_gap
                    backward_level[node] = node.get_lvl()
                    backward_dist[node] = backward_dist[v]
                    continue
                else:
                    b_gap = math.inf

            if (b_gap != math.inf) and (v.get_lvl() == (len(hh)-1)):
                backward_core.add(v)
                for u in forward_core:
                    new_dist = (forward_dist[u] + hh[-1].d_l[(u,v)] + backward_dist[v])
                    if t_length > new_dist:
                        t_length = new_dist
                continue

            if backward_dist[v] + get_pot(start_node,get_init(v),backward_pot) > t_length:
                b_flag = False
                break

            for x in v.get_incoming_nodes():

                level = backward_level[v]
                l = hh[level].get_edge_length(x, v)
                gap = b_gap 

                saver_v = v
                while (l > gap) and (level < (len(hh)-1)):
                    level += 1
                    gap = hh[level].get_b_nachbarschaft(saver_v.get_up())
                    saver_v = saver_v.get_up()
                

                if (level > backward_level[v]) and (already_backward.get(v) == None) and (gap != math.inf):
                    node = v.get_up()
                    backward_queue.update(node,backward_dist[v])
                    already_backward[v] = 1
                    backward_gap[node] = gap
                    backward_level[node] = level
                    backward_dist[node] = backward_dist[v]
                    continue
                

                if (gap != math.inf) and (level == (len(hh)-1)) and (level > backward_level[v]):
                    backward_dist[saver_v] = backward_dist[v]
                    backward_core.add(saver_v)
                    for u in forward_core:
                        new_dist = (forward_dist[u] + hh[-1].d_l[(u,saver_v)] + backward_dist[saver_v])
                        if t_length > new_dist:
                            t_length = new_dist
                    continue
                
                if x not in backward_visited and (backward_dist[x] > backward_dist_v + l):
                    back_update_distances(
                        v,
                        x, 
                        backward_dist,
                        l,
                        backward_queue,
                        backward_gap,
                        backward_level,
                        gap,
                        x.get_lvl(),
                        backward_pot,
                        start_node,
                    )

    return t_length
