import random
from Algorithmen.SSSP_Dijkstra import sssp_dijkstra_algorithm

def random_landmarks(graph, anzahl):
    """
    input: Ein Graph graph und eine Anzahl von Landmarken 
    output: Eine Potenzfunktion (ALT)
    """
    landmarks = []
    dist = []
    r = []
    vert = graph.get_vertices()
    while len(landmarks) != anzahl:
        ra = random.randint(0,len(vert)-1)
        if ra not in r:
            landmarks.append(vert[ra])
            r.append(ra)

    for l in landmarks: 
        dist.append(sssp_dijkstra_algorithm(graph, l))
    
    return dist
    