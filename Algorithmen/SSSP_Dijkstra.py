from PriorityQueue.MinHeap import MinHeap


def initialize(graph, start_node, dist, prio_queue):
    infinity = float("inf")
    for v in graph.get_vertices():
        dist[v] = infinity
    dist[start_node] = 0
    prio_queue.insert(start_node, 0)


def update_distance(u, v, dist, edge_length, queue):
    new_dist = dist[u] + edge_length
    if new_dist < dist[v]:
        dist[v] = new_dist
        queue.update(v, new_dist)


def forward_algorithm(graph, start_node):
    dist = {}
    queue = MinHeap()
    visited = {}
    initialize(graph, start_node, dist, queue)
    while queue.isEmpty() == False:
        u = queue.getMin()
        visited[u] = 1
        for v in u.get_adjacent_nodes():
            if v not in visited:
                update_distance(u, v, dist, graph.get_edge_length(u, v), queue)
    return dist

def backward_algorithm(graph, start_node):
    dist = {}
    queue = MinHeap()
    visited = {}
    initialize(graph, start_node, dist, queue)
    while queue.isEmpty() == False:
        u = queue.getMin()
        visited[u] = 1
        for v in u.get_incoming_nodes():
            if v not in visited:
                update_distance(u, v, dist, graph.get_edge_length(v, u), queue)
    return dist

def sssp_dijkstra_algorithm(graph, start_node):
    out_dist = forward_algorithm(graph, start_node)
    inc_dist = backward_algorithm(graph, start_node)
    return (out_dist,inc_dist)