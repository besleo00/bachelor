from PriorityQueue.MinHeap import MinHeap
import math


def initialize(graph, start_node, dist, pred, prio_queue):
    infinity = float("inf")
    for v in graph.get_vertices():
        dist[v] = infinity
        pred[v] = 0
    dist[start_node] = 0
    prio_queue.insert(start_node, 0)


def update_distance(u, v, dist, pred, edge_length, queue):
    new_dist = dist[u] + edge_length
    if new_dist < dist[v]:
        dist[v] = new_dist
        pred[v] = u
        queue.update(v, new_dist)


def dijkstra_algorithm(graph, start_node, end_node):
    dist = {}
    pred = {}
    queue = MinHeap()
    visited = {}
    suchraum = 0
    if start_node == end_node:
        return 0
    initialize(graph, start_node, dist, pred, queue)
    while queue.isEmpty() == False:
        u = queue.getMin()
        suchraum += 1
        visited[u] = 1
        for v in u.get_adjacent_nodes():
            if v not in visited:
                update_distance(u, v, dist, pred, graph.get_edge_length(u, v), queue)
                if v == end_node:
                    return dist[v]
    return dist[end_node]
    
