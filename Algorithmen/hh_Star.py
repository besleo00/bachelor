from PriorityQueue.MinHeap import MinHeap
from Datenstruktur.Tree import Shortest_Path_Tree
from Datenstruktur.Graph import Directed_Graph
from Datenstruktur.Edge import Edge
from Datenstruktur.Node import Node
from Algorithmen.SSSP_Dijkstra import forward_algorithm
import math

def dij_initialize(graph, start_node, dist, prio_queue):
    infinity = float("inf")
    for v in graph.get_vertices():
        dist[v] = infinity
    dist[start_node] = 0
    prio_queue.insert(start_node, 0)

def dij_initialize2(graph, start_node, dist, pred, prio_queue):
    infinity = float("inf")
    for v in graph.get_vertices():
        dist[v] = infinity
        pred[v] = 0
    dist[start_node] = 0
    prio_queue.insert(start_node, 0)


def dij_update_distance(u, v, dist, edge_length, queue):
    new_dist = dist[u] + edge_length
    if new_dist < dist[v]:
        dist[v] = new_dist
        queue.update(v, new_dist)

def dij_update_distance2(u, v, dist, pred, edge_length, queue):
    new_dist = dist[u] + edge_length
    if new_dist < dist[v]:
        dist[v] = new_dist
        pred[v] = u
        queue.update(v, new_dist)

def dijkstra_algorithm_neighborhood(graph, start_node, h):
    """
    Input: Ein Graph graph, ein Knoten start_node und eine Nachbarschaftsgröße h
    Output: Berechnet die H-Nachbarschaft von start_node im Graphen graph
    """
    dist = {}
    queue = MinHeap()
    visited = {}
    neighborhood = []
    if start_node == None:
        return (math.inf,set())
    queue.update(start_node,0)
    dist[start_node] = 0
    while queue.isEmpty() == False:
        u = queue.getMin()
        visited[u] = 1
        neighborhood.append(u)
        if len(visited) >= h:
            return (dist[u],set(neighborhood))
        for v in u.get_adjacent_nodes():
            if v not in visited:
                l = graph.get_edge_length(u, v)
                new_dist = dist[u] + l
                prev_dist = dist.get(v)
                if prev_dist == None:
                    dist[v] = new_dist
                    queue.update(v,new_dist)
                elif (new_dist < prev_dist):
                    dist[v] = new_dist
                    queue.update(v,new_dist)
    return (dist[neighborhood[-1]],set(neighborhood))

def dijkstra_algorithm_neighborhood_rev(graph, start_node, h):
    """
    Input: Ein Graph graph, ein Knoten start_node und eine Nachbarschaftsgröße h
    Output: Berechnet die H-Nachbarschaft von start_node im Rückwärtsgraphen von graph
    """
    dist = {}
    queue = MinHeap()
    visited = {}
    neighborhood = []
    if start_node == None:
        return (math.inf,set())
    queue.update(start_node,0)
    dist[start_node] = 0
    while queue.isEmpty() == False:
        u = queue.getMin()
        visited[u] = 1
        neighborhood.append(u)
        if len(visited) >= h:
            return (dist[u],set(neighborhood))
        for v in u.get_incoming_nodes():
            if v not in visited:
                l = graph.get_edge_length(v, u)
                new_dist = dist[u] + l
                prev_dist = dist.get(v)
                if prev_dist == None:
                    dist[v] = new_dist
                    queue.update(v,new_dist)
                elif (new_dist < prev_dist):
                    dist[v] = new_dist
                    queue.update(v,new_dist)
    return (dist[neighborhood[-1]],set(neighborhood))

def dijkstra_algorithm_network(graph, start_node, h_neigh, f=0):
    """
    Input: Ein Graph graph, ein Knoten start_node, die H-Nachbarschaft von graph und die Einzelgägnerkostante f
    Output: Berechnet einen partiellen Kürzeste-Wege-Baum mit start_node als Wurzel
    """
    dist = {}
    pred = {}
    queue = MinHeap()
    tree = Shortest_Path_Tree()
    tree.set_root(start_node)
    visited = {}
    status = {}
    status[start_node] = 1 #1 ist Aktiv und 0 ist Passive
    active_unsettled_set = set()
    active_unsettled_set.add(start_node)
    maverick = set()
    queue.update(start_node,0)
    dist[start_node] = 0
    pred[start_node] = 0
    while queue.isEmpty() == False: 
        u = queue.getMin()
        tree.add_edge(pred[u],u)
        if status[u] == 1:
            active_unsettled_set.remove(u)
            if u in maverick:
                maverick.remove(u)
        if (u != start_node):
            if len(h_neigh[tree.get_s_one(u)][1] & h_neigh[u][1]) <= 1:
                status[u] = 0 #Auf Passiv setzen
        visited[u] = 1
        for v in u.get_adjacent_nodes():
            if (status[u] == 0) and (len(maverick) == len(active_unsettled_set)): #Einzelgänger-Knoten
                continue
            if v not in visited: 
                l = graph.get_edge_length(u, v)
                new_dist = dist[u] + l
                prev_dist = dist.get(v)
                if prev_dist == None:
                    dist[v] = new_dist
                    pred[v] = u
                    queue.update(v,new_dist)
                elif (new_dist < prev_dist):
                    dist[v] = new_dist
                    pred[v] = u
                    queue.update(v,new_dist)
                status[v] = status[u]
                if status[v] == 1:
                    active_unsettled_set.add(v)
                    if dist[v] > f * h_neigh[start_node][0]:
                        maverick.add(v)
                else:
                    if v in active_unsettled_set:
                        active_unsettled_set.remove(v)
                        if v in maverick:
                            maverick.remove(v)
        if len(active_unsettled_set) == 0:
            return tree
    return tree

def dijkstra_algorithm_network2(graph, start_node, h_neigh, f=0):
    """ Spezialfall
    Input: Ein Graph graph, ein Knoten start_node, die H-Nachbarschaft von graph und die Einzelgägnerkostante f
    Output: Berechnet einen partiellen Kürzeste-Wege-Baum mit start_node als Wurzel
    """
    dist = {}
    pred = {}
    queue = MinHeap()
    tree = Shortest_Path_Tree()
    tree.set_root(start_node)
    visited = {}
    status = {}
    status[start_node] = 1 #1 ist Aktiv und 0 ist Passive
    active_unsettled_set = set()
    active_unsettled_set.add(start_node)
    maverick = set()
    queue.update(start_node,0)
    dist[start_node] = 0
    pred[start_node] = 0
    while queue.isEmpty() == False: 
        u = queue.getMin()
        tree.add_edge(pred[u],u)
        if status[u] == 1:
            active_unsettled_set.remove(u)
            if u in maverick:
                maverick.remove(u)
        if (u != start_node):
            if len(h_neigh[tree.get_s_one(u).get_down()][1] & h_neigh[u.get_down()][1]) <= 1:
                status[u] = 0 #Auf Passiv setzen
        visited[u] = 1
        for v in u.get_adjacent_nodes():
            if (status[u] == 0) and (len(maverick) == len(active_unsettled_set)): #Einzelgänger-Knoten
                continue
            if v not in visited: 
                l = graph.get_edge_length(u, v)
                new_dist = dist[u] + l
                prev_dist = dist.get(v)
                if prev_dist == None:
                    dist[v] = new_dist
                    pred[v] = u
                    queue.update(v,new_dist)
                elif (new_dist < prev_dist):
                    dist[v] = new_dist
                    pred[v] = u
                    queue.update(v,new_dist)
                status[v] = status[u]
                if status[v] == 1:
                    active_unsettled_set.add(v)
                    if dist[v] > f * h_neigh[start_node.get_down()][0]:
                        maverick.add(v)
                else:
                    if v in active_unsettled_set:
                        active_unsettled_set.remove(v)
                        if v in maverick:
                            maverick.remove(v)
        if len(active_unsettled_set) == 0:
            return tree
    return tree

def get_highway_edges(neighborhood, root, tree):
    """
    Input: H-Nachbarschaft neighborhood, der Wurzelknoten des Kürzesten-Wege-Baumes tree.
    Output: Highway-Kanten in tree ausgehend von root
    """
    edges = set()
    for l in tree.get_leafs():
        current = l
        while current != 0:
            if((current not in neighborhood[root][1]) and (tree.get_parent(current) not in neighborhood[l][1])): #Laufzeit? Eventuell lieber ein Wörterbuch für die Neighborhood verwenden
                if tree.get_parent(current) != 0:
                    edges.add((tree.get_parent(current),current))
            current = tree.get_parent(current)
    return edges

def get_highway_edges2(neighborhood, root, tree): #Eventuell hier nochmal gucken, ob alles richtig ist.
    """ Spezialfall
    Input: H-Nachbarschaft neighborhood, der Wurzelknoten des Kürzesten-Wege-Baumes tree.
    Output: Highway-Kanten in tree ausgehend von root
    """
    edges = set()
    for l in tree.get_leafs():
        current = l
        while current != 0:
            if((current not in neighborhood[root.get_down()][1]) and (tree.get_parent(current) not in neighborhood[l.get_down()][1])): #Laufzeit? Eventuell lieber ein Wörterbuch für die Neighborhood verwenden
                if tree.get_parent(current) != 0:
                    edges.add((tree.get_parent(current),current))
            current = tree.get_parent(current)
    return edges

def h_neighborhood(graph, h):
    """
    Input: Ein Graph graph und eine Nachbarschaftsgröße h
    Output: H-Nachbarschaft des Graphen G
    """
    dh = {}
    for v in graph.get_vertices():
        dh[v] = dijkstra_algorithm_neighborhood(graph, v, h)
    return dh

def h_neighborhood_rev(graph, h):
    """
    Input: Ein Graph graph und eine Nachbarschaftsgröße h
    Output: H-Nachbarschaft des Rückwärtsgraphen von graph
    """
    dh = {}
    for v in graph.get_vertices():
        dh[v] = dijkstra_algorithm_neighborhood_rev(graph, v, h)
    return dh

def build_graph(init_graph, graph_plus, edges, bool=False):
    """
    Input: Ein Graph init_graph, ein leerer Graph graph_plus und eine Kantenmenge edges. Bool differenziert zwischen Spezialfällen.
    Output: graph_plus ist eine Kopie von init_graph, welche durch die Kanten in edges definiert wird.
    """
    new_nodes = []
    new_edges = []
    visited = {}
    for e in edges:
        s = e[0]
        t = e[1]
        if (visited.get(s) == None) and (visited.get(t) == None): #Fall 1: s ist nicht behandelt und t auch nicht
            sx, sy = s.get_coords()
            tx, ty = t.get_coords()
            new_source_node = Node(sx,sy)
            new_target_node = Node(tx,ty)
            visited[s] = new_source_node
            visited[t] = new_target_node
            new_source_node.adjacent.append(new_target_node)
            new_target_node.incoming.append(new_source_node)
            l = init_graph.get_edge_length(s,t)
            copy_edge = Edge(new_source_node,new_target_node,l)
            graph_plus.edge_index[(new_source_node,new_target_node)] = copy_edge
            graph_plus.edge_lengths[(new_source_node,new_target_node)] = l
            if bool:
                new_source_node.set_lvl(s.get_lvl())
                new_target_node.set_lvl(t.get_lvl())
            else:
                new_source_node.set_lvl(s.get_lvl()+1)
                new_target_node.set_lvl(t.get_lvl()+1)
                s.set_up(new_source_node)
                t.set_up(new_target_node)
            new_source_node.set_down(s)
            new_target_node.set_down(t)
            new_nodes.append(new_source_node)
            new_nodes.append(new_target_node)
            new_edges.append(copy_edge)
        elif (visited.get(s) == None) and (visited.get(t) != None): #Fall 2: s ist nicht behandelt. t aber schon
            sx, sy = s.get_coords()
            new_source_node = Node(sx,sy)
            visited[s] = new_source_node
            new_target_node = visited[t]
            new_source_node.adjacent.append(new_target_node)
            new_target_node.incoming.append(new_source_node)
            l = init_graph.get_edge_length(s,t)
            copy_edge = Edge(new_source_node,new_target_node,l)
            graph_plus.edge_index[(new_source_node,new_target_node)] = copy_edge
            graph_plus.edge_lengths[(new_source_node,new_target_node)] = l
            if bool:
                new_source_node.set_lvl(s.get_lvl())
            else:
                new_source_node.set_lvl(s.get_lvl()+1)
                s.set_up(new_source_node)
            new_source_node.set_down(s)
            new_nodes.append(new_source_node)
            new_edges.append(copy_edge)
        elif (visited.get(t) == None) and (visited.get(s) != None): #Fall 3: s ist behandelt und t nicht
            tx, ty = t.get_coords()
            new_target_node = Node(tx,ty)
            visited[t] = new_target_node
            new_source_node = visited[s]
            new_source_node.adjacent.append(new_target_node)
            new_target_node.incoming.append(new_source_node)
            l = init_graph.get_edge_length(s,t)
            copy_edge = Edge(new_source_node,new_target_node,l)
            graph_plus.edge_index[(new_source_node,new_target_node)] = copy_edge
            graph_plus.edge_lengths[(new_source_node,new_target_node)] = l
            if bool:
                new_target_node.set_lvl(t.get_lvl())
            else:
                new_target_node.set_lvl(t.get_lvl()+1)
                t.set_up(new_target_node)
            new_target_node.set_down(t)
            new_nodes.append(new_target_node)
            new_edges.append(copy_edge)
        else: #Fall 4: Beide sind behandelt
            new_source_node = visited[s]
            new_target_node = visited[t]
            new_source_node.adjacent.append(new_target_node)
            new_target_node.incoming.append(new_source_node)
            l = init_graph.get_edge_length(s,t)
            copy_edge = Edge(new_source_node,new_target_node,l)
            graph_plus.edge_lengths[(new_source_node,new_target_node)] = l
            graph_plus.edge_index[(new_source_node,new_target_node)] = copy_edge
            new_edges.append(copy_edge)

    return graph_plus.build_graph(new_nodes,new_edges)

def build_graph_contracted(init_graph, graph_plus, edges):
    """ Für die Kontraktion
    Input: Ein Graph init_graph, ein leerer Graph graph_plus und eine Kantenmenge edges.
    Output: graph_plus ist eine Kopie von init_graph, welche durch die Kanten in edges definiert wird.
    """
    new_nodes = []
    new_edges = []
    visited = {}
    for e in edges: #Wollen eine Kopie erstellen
        s = e[0]
        t = e[1]
        if (visited.get(s) == None) and (visited.get(t) == None): #Fall 1: s ist nicht behandelt und t auch nicht
            sx, sy = s.get_coords()
            tx, ty = t.get_coords()
            new_source_node = Node(sx,sy)
            new_target_node = Node(tx,ty)
            visited[s] = new_source_node
            visited[t] = new_target_node
            new_source_node.adjacent.append(new_target_node)
            new_target_node.incoming.append(new_source_node)
            copy_edge = Edge(new_source_node,new_target_node,init_graph.get_edge_length(s,t))
            graph_plus.edge_lengths[(new_source_node,new_target_node)] = init_graph.get_edge_length(s,t)
            graph_plus.edge_index[(new_source_node,new_target_node)] = copy_edge
            new_source_node.set_down(s.get_down())
            new_target_node.set_down(t.get_down())
            s.get_down().set_up(new_source_node)
            t.get_down().set_up(new_target_node)
            new_source_node.set_lvl(s.get_lvl()+1)
            new_target_node.set_lvl(t.get_lvl()+1)
            new_nodes.append(new_source_node)
            new_nodes.append(new_target_node)
            new_edges.append(copy_edge)
        elif (visited.get(s) == None) and (visited.get(t) != None): #Fall 2: s ist nicht behandelt. t aber schon
            sx, sy = s.get_coords()
            new_source_node = Node(sx,sy)
            visited[s] = new_source_node
            new_target_node = visited[t]
            new_source_node.adjacent.append(new_target_node)
            new_target_node.incoming.append(new_source_node)
            copy_edge = Edge(new_source_node,new_target_node,init_graph.get_edge_length(s,t))
            graph_plus.edge_lengths[(new_source_node,new_target_node)] = init_graph.get_edge_length(s,t)
            graph_plus.edge_index[(new_source_node,new_target_node)] = copy_edge
            s.set_up(new_source_node)
            new_source_node.set_down(s.get_down())
            s.get_down().set_up(new_source_node)
            new_source_node.set_lvl(s.get_lvl()+1)
            new_nodes.append(new_source_node)
            new_edges.append(copy_edge)
        elif (visited.get(t) == None) and (visited.get(s) != None): #Fall 3: s ist behandelt und t nicht
            tx, ty = t.get_coords()
            new_target_node = Node(tx,ty)
            visited[t] = new_target_node
            new_source_node = visited[s]
            new_source_node.adjacent.append(new_target_node)
            new_target_node.incoming.append(new_source_node)
            copy_edge = Edge(new_source_node,new_target_node,init_graph.get_edge_length(s,t))
            graph_plus.edge_lengths[(new_source_node,new_target_node)] = init_graph.get_edge_length(s,t)
            graph_plus.edge_index[(new_source_node,new_target_node)] = copy_edge
            new_target_node.set_down(t.get_down())
            t.get_down().set_up(new_target_node)
            new_target_node.set_lvl(t.get_lvl()+1)
            new_nodes.append(new_target_node)
            new_edges.append(copy_edge)
        else: #Fall 4: Beide sind behandelt
            new_source_node = visited[s]
            new_target_node = visited[t]
            new_source_node.adjacent.append(new_target_node)
            new_target_node.incoming.append(new_source_node)
            copy_edge = Edge(new_source_node,new_target_node,init_graph.get_edge_length(s,t))
            graph_plus.edge_lengths[(new_source_node,new_target_node)] = init_graph.get_edge_length(s,t)
            graph_plus.edge_index[(new_source_node,new_target_node)] = copy_edge
            new_edges.append(copy_edge)

    return graph_plus.build_graph(new_nodes,new_edges)
    
    

def get_highway_network(graph, h_neigh):
    """
    Input: Ein Graph G und die H-Nachbarschaft von G
    Output: Highway-Netzwerk G_1 des Graphen G
    """
    h_edges = set()
    for v in graph.get_vertices():
        tree = dijkstra_algorithm_network(graph, v, h_neigh) #Phase 1
        h_edges = h_edges | get_highway_edges(h_neigh, v, tree) #Phase 2
    return h_edges

def get_highway_network2(graph, h_neigh):
    """
    Spezialfall
    """
    h_edges = set()
    for v in graph.get_vertices():
        tree = dijkstra_algorithm_network2(graph, v, h_neigh) #Phase 1
        h_edges = h_edges | get_highway_edges2(h_neigh, v, tree) #Phase 2
    return h_edges

def get_shortcuts(graph, node):
    """
    Berechnet die Shortcut-Menge
    """
    out = node.get_adjacent_nodes()
    inc = node.get_incoming_nodes()
    shortcuts = []
    for x in inc:
        for y in out:
            if graph.get_edge_length(x,y) > graph.get_edge_length(x,node) + graph.get_edge_length(node,y):
                shortcuts.append((x,y))
    return shortcuts


def get_contracted_hn(graph, c):
    """
    Input: Ein Graph = G und den Kontraktionsparameter c
    Output: Der kontrahierte Graph G'
    """
    stack = []
    removed = {}
    b_nodes = []
    g = Directed_Graph()
    build_graph(graph,g,[e.get_edge() for e in graph.get_edges()], True)
    for v in g.get_vertices():
        stack.append(v)
    while len(stack) != 0:
        current = stack.pop()
        removed[current] = 1
        shortcuts = get_shortcuts(g, current)
        if len(shortcuts) <= c * (len(current.get_adjacent_nodes()) + len(current.get_incoming_nodes())):
            b_nodes.append(current)
            removed[current] = 0
            for s in shortcuts:
                s[0].adjacent.append(s[1])
                s[1].incoming.append(s[0])
                shortcut_edge = Edge(s[0],s[1],g.get_edge_length(s[0],current)+g.get_edge_length(current,s[1]))
                g.edge_lengths[(s[0],s[1])] = g.get_edge_length(s[0],current)+g.get_edge_length(current,s[1])
                g.add_edge(shortcut_edge)
                if removed.get(s[0]) == 1:
                    removed[s[0]] = None
                    stack.append(s[0])
                if removed.get(s[1]) == 1:
                    removed[s[1]] = None
                    stack.append(s[1])
    for b in b_nodes:
        b.get_down().set_bypass()
        g.remove_node(b)
    for v in g.get_vertices():
        v.get_down().set_core()
    return g

def get_distances(graph):
    """
    Berechnet die Werte für die Distanztabelle
    """
    distances = {}
    for u in graph.get_vertices():
        dist = forward_algorithm(graph,u)
        for v in graph.get_vertices():
            distances[(u,v)] = dist[v]
    graph.set_dl(distances)

def hh_preprocessing(graph, H, L): #
    """
    Input: Bekommt einen Graphen, Nachbarschaftsgröße H und eine maximale Ebenenanzahl L
    Output: Berechnet die Highway Hierachie G* = G0, G1, ... , GL-1
    """
    highway_hierarchie = []
    if L == 1:
        return [graph]
    nachbarschaft = h_neighborhood(graph,H)
    graph.set_f_nachbarschaft(nachbarschaft)
    graph.set_b_nachbarschaft(h_neighborhood_rev(graph,H))
    g_i = Directed_Graph()
    build_graph(graph, g_i, get_highway_network(graph,nachbarschaft))
    highway_hierarchie.append(graph)
    highway_hierarchie.append(g_i)
    current = g_i
    while len(highway_hierarchie) != L:
        highway_network = get_contracted_hn(current,0.5) #Hier den Kontraktionswert c einstellen
        nachbarschaft = h_neighborhood(current,H)
        current.set_f_nachbarschaft(nachbarschaft)
        current.set_b_nachbarschaft(h_neighborhood_rev(current,H))
        g_i =  Directed_Graph()
        build_graph_contracted(highway_network, g_i, get_highway_network2(highway_network,nachbarschaft))
        highway_hierarchie.append(g_i)
        current = g_i
        if (abs(len(highway_hierarchie[-1].get_vertices()) - len(highway_hierarchie[-2].get_vertices())) <= 500) or (len(highway_hierarchie[-1].get_vertices()) <= 5000):
            break
    get_distances(highway_hierarchie[-1])
    highway_hierarchie[-1].set_f_nachbarschaft(h_neighborhood(highway_hierarchie[-1],H))
    highway_hierarchie[-1].set_b_nachbarschaft(h_neighborhood_rev(highway_hierarchie[-1],H))
    return highway_hierarchie


