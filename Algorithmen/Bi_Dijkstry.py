from PriorityQueue.MinHeap import MinHeap
import math


def initialize(
    graph,
    start_node,
    end_node,
    front_dist,
    back_dist,
    front_queue,
    back_queue,
    front_pred,
    back_pred,
):
    infinity = float("inf")
    for v in graph.get_vertices():
        front_dist[v] = infinity
        back_dist[v] = infinity
        front_pred[v] = 0
        back_pred[v] = 0
    front_dist[start_node] = 0
    back_dist[end_node] = 0
    front_queue.insert(start_node, 0)
    back_queue.insert(end_node, 0)


def front_update_distances(u, v, front_dist, front_pred, edge_length, front_queue):
    new_dist = front_dist[u] + edge_length
    if new_dist < front_dist[v]:
        front_dist[v] = new_dist
        front_pred[v] = u
        front_queue.update(v, new_dist)


def back_update_distances(u, v, back_dist, back_pred, edge_length, back_queue):
    new_dist = back_dist[u] + edge_length
    if new_dist < back_dist[v]:
        back_dist[v] = new_dist
        back_pred[v] = u
        back_queue.update(v, new_dist)


def construct_path(node_pair, front, back, graph):
    forward = []
    backward = []
    if node_pair[2] == 0:
        x, u = node_pair[0:2]
        forward.append(u)
        backward.append(x)
        u = front[u]
        while u != 0:
            forward.append(u)
            u = front[u]
        x = back[x]
        while x != 0:
            backward.append(x)
            x = back[x]
    else:
        x, v = node_pair[0:2]
        forward.append(x)
        backward.append(v)
        x = front[x]
        while x != 0:
            forward.append(x)
            x = front[x]
        v = back[v]
        while v != 0:
            backward.append(v)
            v = back[v]
    p = forward[::-1] + backward
    r = []
    for x in range(len(p) - 1):
        if graph.get_edge_length(p[x], p[x + 1]) == math.inf:
            r.append((p[x].get_coords(), p[x + 1].get_coords()))
    return r


def bidirectional_dijkstra(graph, start_node, end_node):

    if start_node == end_node:
        return 0

    t_length = float("inf")
    forward_dist = {}
    backward_dist = {}
    forward_visited = {}
    backward_visited = {}
    forward_pred = {}
    backward_pred = {}
    forward_queue = MinHeap()
    backward_queue = MinHeap()
    suchraum = 0

    initialize(
        graph,
        start_node,
        end_node,
        forward_dist,
        backward_dist,
        forward_queue,
        backward_queue,
        forward_pred,
        backward_pred,
    )

    while (forward_queue.isEmpty() == False) and (backward_queue.isEmpty() == False):

        u = forward_queue.getMin()
        v = backward_queue.getMin()
        suchraum += 2

        forward_visited[u] = 1
        backward_visited[v] = 1

        backward_dist_v = backward_dist[v]
        forward_dist_u = forward_dist[u]

        for x in u.get_adjacent_nodes():
            l = graph.get_edge_length(u, x)
            backward_dist_x = backward_dist[x]
            if x not in forward_visited and (forward_dist[x] > forward_dist_u + l):
                front_update_distances(
                    u,
                    x,
                    forward_dist,
                    forward_pred,
                    l,
                    forward_queue,
                )

            if (x in backward_visited) and (
                forward_dist_u + l + backward_dist_x < t_length
            ):
                t_length = forward_dist_u + l + backward_dist_x

        for x in v.get_incoming_nodes():
            l = graph.get_edge_length(x, v)
            forward_dist_x = forward_dist[x]
            if x not in backward_visited and (backward_dist[x] > backward_dist_v + l):
                back_update_distances(
                    v,
                    x,
                    backward_dist,
                    backward_pred,
                    l,
                    backward_queue,
                )

            if (x in forward_visited) and (
                backward_dist_v + l + forward_dist_x < t_length
            ):
                t_length = backward_dist_v + l + forward_dist_x

        if forward_dist_u + backward_dist_v >= t_length:
            return t_length

    return t_length
