from PriorityQueue.MinHeap import MinHeap
import math


def initialize(graph, start_node, dist, pred, prio_queue):
    infinity = float("inf")
    for v in graph.get_vertices():
        dist[v] = infinity
        pred[v] = 0
    dist[start_node] = 0
    prio_queue.insert(start_node, 0)


def construct_path(end_node, pred, ve):
    path = []
    p = pred[end_node]
    while p != 0:
        path.append(p)
        p = pred[p]
    return (path[::-1] + [end_node],ve)


def update_distance(u, v, dist, pred, edge_length, queue, pot):
    new_dist = dist[u] + edge_length
    if new_dist < dist[v]:
        dist[v] = new_dist
        pred[v] = u
        queue.update(v, new_dist + pot[v])

def euclidian_pot(graph, end_node):
    pot = {}
    ex, ey = end_node.get_coords()
    for v in graph.get_vertices():
        x,y = v.get_coords()
        pot[v] = round((math.sqrt(math.pow(ex-x,2)+math.pow(ey-y,2)))) 
    return pot


def a_star(graph, start_node, end_node):
    dist = {}
    pred = {}
    queue = MinHeap()
    visited = {}
    visited_edges = []
    pot = euclidian_pot(graph, end_node)
    initialize(graph, start_node, dist, pred, queue)
    while queue.isEmpty() == False:
        u = queue.getMin()
        visited[u] = 1
        if pred[u] != 0:
            visited_edges.append((pred[u].get_coords(),u.get_coords()))
        for v in u.get_adjacent_nodes():
            if v not in visited:
                update_distance(u, v, dist, pred, graph.get_edge_length(u, v), queue, pot)
                if v == end_node:
                    #return construct_path(end_node, pred, visited_edges)
                    return dist[end_node]
                    #return visited_edges
    if pred[end_node] != 0:
        #return construct_path(end_node, pred, visited_edges)
        return dist[end_node]
        #return visited_edges
    elif start_node == end_node:
        return 0
    else:
        return -1
