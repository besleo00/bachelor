from PriorityQueue.MinHeap import MinHeap
from Algorithmen.Landmarks.Landmarks_ALT import random_landmarks

def initialize(graph, start_node, dist, pred, prio_queue):
    infinity = float("inf")
    for v in graph.get_vertices():
        dist[v] = infinity
        pred[v] = 0
    dist[start_node] = 0
    prio_queue.insert(start_node, 0)

def update_distance(u, v, dist, pred, edge_length, queue, end_node, pot):
    new_dist = dist[u] + edge_length
    if new_dist < dist[v]:
        dist[v] = new_dist
        pred[v] = u
        queue.update(v, new_dist + get_pot(v,end_node,pot))

def get_pot(node, end_node, pot):
    result_list = []
    for out, inc in pot: ###
        result_list.append(max(inc[node]-inc[end_node],out[end_node]-out[node]))
    return max(result_list)

def alt_algorithm(graph, start_node, end_node, pot):
    dist = {}
    pred = {}
    queue = MinHeap()
    visited = {}
    visited_edges = []
    edges = []
    suchraum = 0
    if start_node == end_node:
        return 0
    initialize(graph, start_node, dist, pred, queue)
    while queue.isEmpty() == False:
        u = queue.getMin()
        suchraum += 1
        edges.append((pred[u],u))
        visited[u] = 1
        if pred[u] != 0:
            visited_edges.append((pred[u].get_coords(),u.get_coords()))
        for v in u.get_adjacent_nodes():
            if v not in visited:
                update_distance(u, v, dist, pred, graph.get_edge_length(u, v), queue, end_node, pot)
                if v == end_node:
                    return dist[v]
    return dist[end_node]
