from PriorityQueue.MinHeap import MinHeap
from Datenstruktur.Graph import Directed_Graph
from Datenstruktur.Edge import Edge
from Datenstruktur.Node import Node
import math

def build_graph(init_graph, graph_plus, edges):
    new_nodes = []
    new_edges = []
    visited = {}
    for e in edges:
        s = e[0]
        t = e[1]
        if (visited.get(s) == None) and (visited.get(t) == None): #Fall 1: s ist nicht behandelt und t auch nicht
            sx, sy = s.get_coords()
            tx, ty = t.get_coords()
            new_source_node = Node(sx,sy)
            new_target_node = Node(tx,ty)
            visited[s] = new_source_node
            visited[t] = new_target_node
            new_source_node.adjacent.append(new_target_node)
            new_target_node.incoming.append(new_source_node)
            copy_edge = Edge(new_source_node,new_target_node,init_graph.get_edge_length(s,t))
            graph_plus.edge_lengths[(new_source_node,new_target_node)] = init_graph.get_edge_length(s,t)
            graph_plus.edge_index[(new_source_node,new_target_node)] = copy_edge
            s.set_up(new_source_node)
            t.set_up(new_target_node)
            new_source_node.set_down(s)
            new_target_node.set_down(t)
            new_nodes.append(new_source_node)
            new_nodes.append(new_target_node)
            new_edges.append(copy_edge)
        elif (visited.get(s) == None) and (visited.get(t) != None): #Fall 2: s ist nicht behandelt. t aber schon
            sx, sy = s.get_coords()
            new_source_node = Node(sx,sy)
            visited[s] = new_source_node
            new_target_node = visited[t]
            new_source_node.adjacent.append(new_target_node)
            new_target_node.incoming.append(new_source_node)
            copy_edge = Edge(new_source_node,new_target_node,init_graph.get_edge_length(s,t))
            graph_plus.edge_lengths[(new_source_node,new_target_node)] = init_graph.get_edge_length(s,t)
            graph_plus.edge_index[(new_source_node,new_target_node)] = copy_edge
            s.set_up(new_source_node)
            new_source_node.set_down(s)
            new_nodes.append(new_source_node)
            new_edges.append(copy_edge)
        elif (visited.get(t) == None) and (visited.get(s) != None): #Fall 3: s ist behandelt und t nicht
            tx, ty = t.get_coords()
            new_target_node = Node(tx,ty)
            visited[t] = new_target_node
            new_source_node = visited[s]
            new_source_node.adjacent.append(new_target_node)
            new_target_node.incoming.append(new_source_node)
            copy_edge = Edge(new_source_node,new_target_node,init_graph.get_edge_length(s,t))
            graph_plus.edge_lengths[(new_source_node,new_target_node)] = init_graph.get_edge_length(s,t)
            graph_plus.edge_index[(new_source_node,new_target_node)] = copy_edge
            t.set_up(new_target_node)
            new_target_node.set_down(t)
            new_nodes.append(new_target_node)
            new_edges.append(copy_edge)
        else: #Fall 4: Beide sind behandelt
            new_source_node = visited[s]
            new_target_node = visited[t]
            new_source_node.adjacent.append(new_target_node)
            new_target_node.incoming.append(new_source_node)
            copy_edge = Edge(new_source_node,new_target_node,init_graph.get_edge_length(s,t))
            graph_plus.edge_lengths[(new_source_node,new_target_node)] = init_graph.get_edge_length(s,t)
            graph_plus.edge_index[(new_source_node,new_target_node)] = copy_edge
            new_edges.append(copy_edge)

    return graph_plus.build_graph(new_nodes,new_edges)

def get_shortcuts(graph, node):
    out = node.get_adjacent_nodes()
    inc = node.get_incoming_nodes()
    shortcuts = []
    for x in inc:
        for y in out:
            if graph.get_edge_length(x,y) > graph.get_edge_length(x,node) + graph.get_edge_length(node,y):
                shortcuts.append((x,y))
    return shortcuts

def get_core(graph, c):
    """
    Input: Ein Graph graph und ein Kontraktionsparameter c
    Output: Kern des Graphen graph
    """
    stack = []
    removed = {}
    b_nodes = []
    g = Directed_Graph()
    build_graph(graph,g,[e.get_edge() for e in graph.get_edges()])
    for v in g.get_vertices():
        stack.append(v)
    while len(stack) != 0:
        current = stack.pop()
        removed[current] = 1
        shortcuts = get_shortcuts(g, current)
        if len(shortcuts) <= c * (len(current.get_adjacent_nodes()) + len(current.get_incoming_nodes())):
            b_nodes.append(current)
            removed[current] = 0
            for s in shortcuts:
                e = g.get_edge(s[0],s[1])
                if e != None:
                    g.remove_edge(e)
                s[0].adjacent.append(s[1])
                s[1].incoming.append(s[0])
                shortcut_edge = Edge(s[0],s[1],g.get_edge_length(s[0],current)+g.get_edge_length(current,s[1]))
                g.edge_lengths[(s[0],s[1])] = g.get_edge_length(s[0],current)+g.get_edge_length(current,s[1])
                g.edge_index[(s[0],s[1])] = shortcut_edge
                g.add_edge(shortcut_edge)
                if removed.get(s[0]) == 1:
                    removed[s[0]] = None
                    stack.append(s[0])
                if removed.get(s[1]) == 1:
                    removed[s[1]] = None
                    stack.append(s[1])
    for b in b_nodes:
        b.get_down().set_up(None)
        g.remove_node(b)
    return g


def initialize(
    graph,
    start_node,
    end_node,
    front_dist,
    back_dist,
    front_queue,
    back_queue,
):
    infinity = float("inf")
    for v in graph.get_vertices():
        front_dist[v] = infinity
        back_dist[v] = infinity
    front_dist[start_node] = 0
    back_dist[end_node] = 0
    front_queue.insert(start_node, 0)
    back_queue.insert(end_node, 0)


def front_update_distances(u, v, front_dist, edge_length, front_queue):
    new_dist = front_dist[u] + edge_length
    if new_dist < front_dist[v]:
        front_dist[v] = new_dist
        front_queue.update(v, new_dist)

def back_update_distances(u, v, back_dist, edge_length, back_queue):
    new_dist = back_dist[u] + edge_length
    if new_dist < back_dist[v]:
        back_dist[v] = new_dist
        back_queue.update(v, new_dist)

def front_update_distances_core(u, v, front_dist, edge_length, front_queue, pot,pp, proxy_start, proxy_end):
    new_dist = front_dist[u] + edge_length
    if new_dist < front_dist[v]:
        front_dist[v] = new_dist
        front_queue.update(v, new_dist+forward_pot(v,proxy_start,proxy_end,pot,pp))

def back_update_distances_core(u, v, back_dist, edge_length, back_queue, pot,pp, proxy_start, proxy_end):
    new_dist = back_dist[u] + edge_length
    if new_dist < back_dist[v]:
        back_dist[v] = new_dist
        back_queue.update(v, new_dist+backward_pot(v,proxy_start,proxy_end,pot,pp))
    
def get_pot(node, end_node, pot):
    result_list = []
    for out, inc in pot: ###
        result_list.append(max(inc[node]-inc[end_node],out[end_node]-out[node]))
    return max(result_list)

def forward_pot(node, start_node, end_node, pot, pp): #Konsistent machen
    return (0.5)*(get_pot(node, end_node, pot) - get_pot(start_node,node,pot)) + pp

def backward_pot(node, start_node, end_node, pot, pp): #Konsistent machen
    return (0.5)*(get_pot(start_node,node,pot) - get_pot(node, end_node,pot)) + pp

def core_alt_query(graph, core, start_node, end_node, pot, proxy_start, proxy_end):

    if start_node == end_node:
        return 0

    t_length = float("inf")
    forward_dist = {}
    backward_dist = {}
    forward_visited = {}
    backward_visited = {}
    forward_queue = MinHeap()
    backward_queue = MinHeap()
    forward_core = set()
    backward_core = set()
    backward_core_dst = math.inf
    forward_core_dst = math.inf

    initialize(
        graph,
        start_node,
        end_node,
        forward_dist,
        backward_dist,
        forward_queue,
        backward_queue,
    )
    prt = get_pot(proxy_end, proxy_start, pot)*(0.5)
    pfs = get_pot(proxy_start,proxy_end,pot)*(0.5)
    p = (0.5)*get_pot(proxy_end,proxy_start,pot) + pfs
    while (forward_queue.isEmpty() == False) or (backward_queue.isEmpty() == False):
        if(forward_queue.isEmpty() == False):
            u = forward_queue.getMin()

            if u.get_up() != None: 
                dst = forward_dist[u]
                forward_core.add((u.get_up(),dst))
                if dst < forward_core_dst:
                    forward_core_dst = dst
                continue

            forward_visited[u] = 1
            forward_dist_u = forward_dist[u]

            for x in u.get_adjacent_nodes():

                l = graph.get_edge_length(u, x)
                backward_dist_x = backward_dist[x]
                if x not in forward_visited and (forward_dist[x] > forward_dist_u + l):
                    front_update_distances(
                        u,
                        x,
                        forward_dist,
                        l,
                        forward_queue,
                    )

                if (x in backward_visited) and (
                    forward_dist_u + l + backward_dist_x < t_length
                ):
                    t_length = forward_dist_u + l + backward_dist_x

        if(backward_queue.isEmpty() == False):
            v = backward_queue.getMin()

            if v.get_up() != None:
                dst = backward_dist[v]
                backward_core.add((v.get_up(),dst))
                if dst < backward_core_dst:
                    backward_core_dst = dst
                continue

            backward_visited[v] = 1
            backward_dist_v = backward_dist[v]


            for x in v.get_incoming_nodes():
               l = graph.get_edge_length(x, v)
               forward_dist_x = forward_dist[x]
               if x not in backward_visited and (backward_dist[x] > backward_dist_v + l):
                   back_update_distances(
                       v,
                       x,
                       backward_dist,
                       l,
                       backward_queue,
                   )

               if (x in forward_visited) and (
                   backward_dist_v + l + forward_dist_x < t_length
               ):
                   t_length = backward_dist_v + l + forward_dist_x


    #Prioritätswarteschlangen sind leer
    for v in core.get_vertices():
        forward_dist[v] = math.inf
        backward_dist[v] = math.inf
    for key,prio in forward_core:
        forward_queue.update(key,prio+forward_pot(key,proxy_start,proxy_end,pot,prt))
        forward_dist[key] = prio
    for key,prio in backward_core:
        backward_queue.update(key,prio+backward_pot(key,proxy_start,proxy_end,pot,pfs))
        backward_dist[key] = prio
    while (forward_queue.isEmpty() == False) or (backward_queue.isEmpty() == False):
        if(forward_queue.isEmpty() == False):
            u = forward_queue.getMin()
            forward_visited[u] = 1
            forward_dist_u = forward_dist[u]

            for x in u.get_adjacent_nodes():
                l = core.get_edge_length(u, x)
                backward_dist_x = backward_dist[x]
                if x not in forward_visited and (forward_dist[x] > forward_dist_u + l):
                    front_update_distances_core(
                        u,
                        x,
                        forward_dist,
                        l,
                        forward_queue,
                        pot,
                        prt,
                        proxy_start,
                        proxy_end,
                    )

                if (x in backward_visited) and (
                    forward_dist_u + l + backward_dist_x < t_length
                ):
                    t_length = forward_dist_u + l + backward_dist_x

        if(backward_queue.isEmpty() == False):
            v = backward_queue.getMin()

            backward_visited[v] = 1
            backward_dist_v = backward_dist[v]

            for x in v.get_incoming_nodes():
               l = core.get_edge_length(x, v)
               forward_dist_x = forward_dist[x]
               if x not in backward_visited and (backward_dist[x] > backward_dist_v + l):
                   back_update_distances_core(
                       v,
                       x,
                       backward_dist,
                       l,
                       backward_queue,
                       pot,
                       pfs,
                       proxy_start,
                       proxy_end,
                   )
               if (x in forward_visited) and (
                   backward_dist_v + l + forward_dist_x < t_length
               ):
                   t_length = backward_dist_v + l + forward_dist_x

        if forward_dist_u + backward_dist_v >= t_length + p:
            return t_length
    
    return t_length

