from PriorityQueue.MinHeap import MinHeap
from Algorithmen.Landmarks.Landmarks_ALT import random_landmarks
import math


def initialize(
    graph,
    start_node,
    end_node,
    front_dist,
    back_dist,
    front_queue,
    back_queue,
    front_pred,
    back_pred,
):
    infinity = float("inf")
    for v in graph.get_vertices():
        front_dist[v] = infinity
        back_dist[v] = infinity
        front_pred[v] = 0
        back_pred[v] = 0
    front_dist[start_node] = 0
    back_dist[end_node] = 0
    front_queue.insert(start_node, 0)
    back_queue.insert(end_node, 0)


def front_update_distances(u, v, front_dist, front_pred, edge_length, front_queue, pot, end_node, start_node):
    new_dist = front_dist[u] + edge_length
    if new_dist < front_dist[v]:
        front_dist[v] = new_dist
        front_pred[v] = u
        front_queue.update(v, new_dist + forward_pot(v,end_node,start_node,pot))


def back_update_distances(u, v, back_dist, back_pred, edge_length, back_queue, pot, end_node, start_node):
    new_dist = back_dist[u] + edge_length
    if new_dist < back_dist[v]:
        back_dist[v] = new_dist
        back_pred[v] = u
        back_queue.update(v, new_dist + backward_pot(v,end_node,start_node,pot))


def get_pot(node, end_node, pot):
    result_list = []
    for out, inc in pot: ###
        result_list.append(max(inc[node]-inc[end_node],out[end_node]-out[node]))
    return max(result_list)

def forward_pot(node, end_node, start_node, pot):
    return (0.5)*(get_pot(node, end_node,pot) - get_pot(start_node,node,pot))

def backward_pot(node, end_node, start_node, pot):
    return (0.5)*(get_pot(start_node,node,pot) - get_pot(node,end_node,pot))

def bidirectional_alt(graph, start_node, end_node, pot):

    if start_node == end_node:
        return 0

    t_length = float("inf")
    forward_dist = {}
    backward_dist = {}
    forward_visited = {}
    backward_visited = {}
    forward_pred = {}
    backward_pred = {}
    forward_queue = MinHeap()
    backward_queue = MinHeap()
    initialize(
        graph,
        start_node,
        end_node,
        forward_dist,
        backward_dist,
        forward_queue,
        backward_queue,
        forward_pred,
        backward_pred,
    )
    while (forward_queue.isEmpty() == False) and (backward_queue.isEmpty() == False):

        u = forward_queue.getMin()
        v = backward_queue.getMin()
        forward_visited[u] = 1
        backward_visited[v] = 1

        backward_dist_v = backward_dist[v]
        forward_dist_u = forward_dist[u]

        for x in u.get_adjacent_nodes():
            l = graph.get_edge_length(u, x)
            backward_dist_x = backward_dist[x]
            if x not in forward_visited and (forward_dist[x] > forward_dist_u + l):
                front_update_distances(
                    u,
                    x,
                    forward_dist,
                    forward_pred,
                    l,
                    forward_queue,
                    pot,
                    end_node,
                    start_node,
                )

            if (x in backward_visited) and (
                forward_dist_u + l + backward_dist_x < t_length
            ):
                t_length = forward_dist_u + l + backward_dist_x

        for x in v.get_incoming_nodes():
            l = graph.get_edge_length(x, v)
            forward_dist_x = forward_dist[x]
            if x not in backward_visited and (backward_dist[x] > backward_dist_v + l):
                back_update_distances(
                    v,
                    x,
                    backward_dist,
                    backward_pred,
                    l,
                    backward_queue,
                    pot,
                    end_node,
                    start_node,
                )

            if (x in forward_visited) and (
                backward_dist_v + l + forward_dist_x < t_length
            ):
                t_length = backward_dist_v + l + forward_dist_x
        if forward_dist_u + forward_pot(u,end_node,start_node,pot) + backward_dist_v + backward_pot(v,end_node,start_node,pot) >= t_length + forward_pot(end_node, end_node, start_node, pot) + backward_pot(end_node, end_node, start_node, pot):
            return t_length

    return t_length
