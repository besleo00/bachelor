import heapq

class MinHeap: #From Algorithm Project --> https://inst.eecs.berkeley.edu/~cs188/fa20/project2/

    def __init__(self):
        self.heap = []
        self.count = 0
    
    def insert(self, key, prio): 
            to_insert = (prio, self.count, key)
            heapq.heappush(self.heap, to_insert)
            self.count += 1
    
    def getMin(self):
        (_,_, min) = heapq.heappop(self.heap)
        return min

    def isEmpty(self):
        return len(self.heap) == 0
    
    def update(self, key, prio):
        for index, (p, c, i) in enumerate(self.heap):
            if i == key:
                if p <= prio:
                    break
                del self.heap[index]
                self.heap.append((prio, c, key))
                heapq.heapify(self.heap)
                break
        else:
            self.insert(key, prio)
    
    def top(self):
        return self.heap[0][0]