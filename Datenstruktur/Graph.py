from Datenstruktur.Edge import Edge
from Datenstruktur.Node import Node
import math
import random

class Directed_Graph:
    
    def __init__(self) -> None:
        self.vertices: list[Node] = []
        self.core: dict = {}
        self.edges: list[Edge] = []
        self.edge_index: dict = {}
        self.edge_lengths: dict = {}
        self.d_l: dict = {}
        self.f_nachbarschaft = {}
        self.b_nachbarschaft = {}

    def add_node(self, node):
        self.vertices.append(node)

    def add_edge(self, edge):
        self.edges.append(edge)
    
    def get_vertices(self):
        return self.vertices

    def get_edges(self):
        return self.edges
    
    def get_edge_length(self,u,v):
        if (u,v) in self.edge_lengths:
            return self.edge_lengths[(u,v)]
        return math.inf
    
    def remove_edge(self,edge):
        if edge == None:
            return
        self.edge_lengths[edge.get_edge()] = None
        if edge in self.edges:
            self.edges.remove(edge)
    
    def remove_node(self, node):
        for x in node.get_adjacent_nodes():
            if node in x.incoming:
                x.incoming.remove(node)
                #self.remove_edge(self.get_edge(node,x))
                self.edge_lengths[(node,x)] = math.inf
        for x in node.get_incoming_nodes():
            if node in x.adjacent:
                x.adjacent.remove(node)
                #self.remove_edge(self.get_edge(x,node))
                self.edge_lengths[(x,node)] = math.inf
        self.vertices.remove(node)
        node.adjacent = []
        node.incoming = []

        
    def get_farthest_x(self):
        c_min = math.inf
        c_max = -math.inf
        c_min_node = None
        c_max_node = None
        saver = []
        for x in self.vertices:
            coord = x.get_coords()[0]

            if coord < c_min:
                c_min_node = x
                c_min = coord
                saver.append(c_min_node)
            if coord > c_max:
                c_max_node = x
                c_max = coord
        return (saver[-2],c_max_node)

    def build_graph(self, vertices, edges):
        for v in vertices:
            self.add_node(v)
        for e in edges:
            self.add_edge(e)


    def get_edge(self, source, target):
        return self.edge_index.get((source,target))

    def set_dl(self,dist):
        self.d_l = dist

    def set_f_nachbarschaft(self,nachbarschaft):
        for v in self.vertices:
            self.f_nachbarschaft[v] = nachbarschaft[v][0]
    
    def get_f_nachbarschaft(self, node):
        element = self.f_nachbarschaft.get(node)
        if element != None:
            return element
        else:
            return math.inf

    def set_b_nachbarschaft(self,nachbarschaft):
        for v in self.vertices:
            self.b_nachbarschaft[v] = nachbarschaft[v][0]
    
    def get_b_nachbarschaft(self, node):
        element = self.b_nachbarschaft.get(node)
        if element != None:
            return element
        else:
            return math.inf
    
    def get_random_nodes(self):
        first = self.vertices[random.randint(0,len(self.vertices)-1)]
        second = self.vertices[random.randint(0,len(self.vertices)-1)]
        while second == first:
            second = self.vertices[random.randint(0,len(self.vertices)-1)]
        return (first,second)

