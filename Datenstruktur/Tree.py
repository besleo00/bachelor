from Datenstruktur.Node import Node

class Shortest_Path_Tree:

    def __init__(self) -> None:
        self.root = None
        self.parent: dict = {}
        self.s_one: dict = {} #Node after root on the path for each node
        self.leafs: list[Node] = []

    def set_root(self, root):
        self.root = root
        self.parent[root] = 0
        self.leafs.append(root)
        self.s_one[root] = None

    def add_edge(self,source, destination): #bottom up
        if source == 0:
            pass
        else:
            self.parent[destination] = source
            if source in self.leafs:
                self.leafs.remove(source)
            self.leafs.append(destination)
            if self.s_one[source] == None: #Is Root
                self.s_one[destination] = destination #Muss s1 sein
            else:
                self.s_one[destination] = self.s_one[source]

    def get_s_one(self, node):
        return self.s_one[node]
    
    def get_leafs(self):
        return self.leafs
    
    def get_parent(self, node):
        return self.parent[node]