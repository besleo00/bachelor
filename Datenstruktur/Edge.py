class Edge:
    
    def __init__(self, src_node, dst_node, length=0, speed=0) -> None:
        self.src_node = src_node
        self.dst_node = dst_node
        self.length = length #Road length
        self.speed = speed #Speed limit for that road-segment
        self.up = None

    def get_source(self):
        return self.src_node
    
    def get_destination(self):
        return self.dst_node
    
    def get_edge(self):
        return (self.src_node, self.dst_node)

    def get_length(self):
        return self.length
    
    def get_up(self):
        return self.up