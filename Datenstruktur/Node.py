from itertools import count as itertools_count

class Node:
    
    node_id = itertools_count() #Credits: https://stackoverflow.com/a/1045724

    def __init__(self, x_coord, y_coord) -> None:
        self.id = next(Node.node_id)
        self.adjacent: list[Node] = []
        self.incoming: list[Node] = []
        self.visited = False
        self.x = x_coord
        self.y = y_coord
        self.core = False
        self.bypass = False
        self.up = None
        self.down = None
        self.n_dst = None 
        self.level = 0 

    def get_adjacent_nodes(self):
        return self.adjacent
    
    def get_incoming_nodes(self):
        return self.incoming
    
    def is_adjacent(self, node):
        return node in self.adjacent
    
    def visit(self):
        self.visited = True
    
    def get_coords(self):
        return (self.x,self.y)

    def is_visited(self):
        return self.visited
    
    def get_up(self):
        return self.up
    
    def set_up(self, node):
        self.up = node

    def get_down(self):
        return self.down

    def set_down(self,node):
        self.down = node

    def get_n_dst(self):
        return self.n_dst

    def set_n_dst(self, n_dst):
        self.n_dst = n_dst

    def set_lvl(self, lvl):
        self.level = lvl

    def get_lvl(self):
        return self.level
    
    def set_core(self):
        self.core = True
    
    def in_core(self):
        return self.core
    
    def set_bypass(self):
        self.bypass = True

    def is_bypassed(self):
        return self.bypass