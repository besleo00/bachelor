from Algorithmen.Dijkstra import dijkstra_algorithm
from Algorithmen.Landmarks.Landmarks_ALT import random_landmarks
from Algorithmen.corealt import get_core
from Algorithmen.corealt import core_alt_query
from Datenstruktur.Graph import Directed_Graph
from PriorityQueue.MinHeap import MinHeap
import unittest
from graphs.get_graph import construct_graph

co_file_path = 'C:\\Users\\besle\Desktop\\Bachelor-Projekt\\graphs\\duesseldorf.co'
gr_file_path ='C:\\Users\\besle\Desktop\\Bachelor-Projekt\\graphs\\duesseldorf.gr'

def get_pot(node, end_node, pot):
    result_list = []
    for out, inc in pot:
        result_list.append(max(inc[node]-inc[end_node],out[end_node]-out[node]))
    return max(result_list)

def get_forward_pot(node, end_node, pot):
    return get_pot(node, end_node, pot)

def get_backward_pot(node, end_node, pot):
    return get_pot(end_node, node, pot)

def forward_pot(node, start_node, end_node, pot, pp): #Konsistent machen
    return (0.5)*(get_pot(node, end_node, pot) - get_pot(start_node,node,pot)) + pp

def backward_pot(node, start_node, end_node, pot, pp): #Konsistent machen
    return (0.5)*(get_pot(start_node,node,pot) - get_pot(node, end_node,pot)) + pp

def get_proxy_forward(graph, start_node):
    dist = {}
    queue = MinHeap()
    visited = {}
    queue.update(start_node,0)
    dist[start_node] = 0
    while queue.isEmpty() == False:
        u = queue.getMin()
        visited[u] = 1
        if u.get_up() != None:
            return u.get_up()
        for v in u.get_adjacent_nodes():
            if v not in visited:
                l = graph.get_edge_length(u, v)
                new_dist = dist[u] + l
                prev_dist = dist.get(v)
                if prev_dist == None:
                    dist[v] = new_dist
                    queue.update(v,new_dist)
                elif (new_dist < prev_dist):
                    dist[v] = new_dist
                    queue.update(v,new_dist)
    return None #Kein Zugang

def get_proxy_backward(graph, start_node):
    dist = {}
    queue = MinHeap()
    visited = {}
    queue.update(start_node,0)
    dist[start_node] = 0
    while queue.isEmpty() == False:
        u = queue.getMin()
        visited[u] = 1
        if u.get_up() != None:
            return u.get_up()
        for v in u.get_incoming_nodes():
            if v not in visited:
                l = graph.get_edge_length(v, u)
                new_dist = dist[u] + l
                prev_dist = dist.get(v)
                if prev_dist == None:
                    dist[v] = new_dist
                    queue.update(v,new_dist)
                elif (new_dist < prev_dist):
                    dist[v] = new_dist
                    queue.update(v,new_dist)
    return None #Kein Zugang

class TestCoreALT(unittest.TestCase):

    def test_core_alt(self):
        g = Directed_Graph()
        construct_graph(g,co_file_path,gr_file_path)
        c = get_core(g, 2.5)
        pot = random_landmarks(c,16)
        x,y = g.get_random_nodes()
        x_proxy = get_proxy_forward(g,x)
        y_proxy = get_proxy_backward(g,y)
        self.assertEqual(dijkstra_algorithm(g,x,y),core_alt_query(g,c,x,y,pot,x_proxy,y_proxy))