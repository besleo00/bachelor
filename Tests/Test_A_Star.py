from Algorithmen.Dijkstra import dijkstra_algorithm
from Algorithmen.ALT import alt_algorithm
from Algorithmen.Bi_A_star import bidirectional_alt
from Algorithmen.Landmarks.Landmarks_ALT import random_landmarks
from graphs.get_graph import construct_graph
from Datenstruktur.Graph import Directed_Graph
import unittest


nodes = 'C:\\Users\\besle\Desktop\\Bachelor-Projekt\\graphs\\spandau.co'
arcs = 'C:\\Users\\besle\Desktop\\Bachelor-Projekt\\graphs\\spandau.gr'

class TestAStar(unittest.TestCase):
    def test_random_paths_uni(self):
        g = Directed_Graph()
        construct_graph(g,nodes,arcs)
        pot = random_landmarks(g,8)
        x, y = g.get_random_nodes()
        self.assertEqual(dijkstra_algorithm(g,x,y), alt_algorithm(g,x,y,pot))
    
    def test_random_paths_bi(self):
        g = Directed_Graph()
        construct_graph(g,nodes,arcs)
        pot = random_landmarks(g,8)
        x, y = g.get_random_nodes()
        self.assertEqual(dijkstra_algorithm(g,x,y), bidirectional_alt(g,x,y,pot))

if __name__ == "__main__":
    unittest.main()
