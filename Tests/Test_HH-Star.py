from Algorithmen.hh_Star import hh_preprocessing
from Algorithmen.hh_star_query import highway_query_algorithm
from Algorithmen.hh_query import highway_query_algorithm2
from Algorithmen.Landmarks.Landmarks_ALT import random_landmarks
from Algorithmen.Dijkstra import dijkstra_algorithm
from Datenstruktur.Graph import Directed_Graph
from graphs.get_graph import construct_graph
import unittest

co_path ='C:\\Users\\besle\Desktop\\Bachelor-Projekt\\graphs\\spandau.co'
gr_path = 'C:\\Users\\besle\Desktop\\Bachelor-Projekt\\graphs\\spandau.gr'


class TestHHStar(unittest.TestCase):

    def test_highway_hierarchie(self):
        g = Directed_Graph()
        construct_graph(g,co_path,gr_path)
        hh = hh_preprocessing(g,30,100)
        x, y = g.get_random_nodes()
        self.assertEqual(dijkstra_algorithm(g,x,y),highway_query_algorithm2(hh,x,y,30))

    def test_highway_hierarchie_star(self):
        g = Directed_Graph()
        construct_graph(g,co_path,gr_path)
        hh = hh_preprocessing(g,30,100)
        x, y = g.get_random_nodes()
        pot = random_landmarks(g,16)
        self.assertEqual(dijkstra_algorithm(g,x,y),highway_query_algorithm(hh,x,y,30,pot))