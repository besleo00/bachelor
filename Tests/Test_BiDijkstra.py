from Algorithmen.Dijkstra import dijkstra_algorithm
from Algorithmen.Bi_Dijkstry import bidirectional_dijkstra
from graphs.get_graph import construct_graph
from Datenstruktur.Graph import Directed_Graph
import unittest

nodes = 'C:\\Users\\besle\Desktop\\Bachelor-Projekt\\graphs\\spandau.co'
arcs = 'C:\\Users\\besle\Desktop\\Bachelor-Projekt\\graphs\\spandau.gr'

class TestBiDikstra(unittest.TestCase):
    def test_random_paths(self):
        g = Directed_Graph()
        construct_graph(g,nodes, arcs)
        x,y = g.get_random_nodes()
        self.assertEqual(dijkstra_algorithm(g,x,y), bidirectional_dijkstra(g,x,y))

if __name__ == "__main__":
    unittest.main()
