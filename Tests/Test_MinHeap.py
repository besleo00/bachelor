import unittest
from PriorityQueue.MinHeap import MinHeap

class TestMinHeap(unittest.TestCase):
    
    def test_insert(self):
        pq = MinHeap()
        loi = [('Key_1',1),('Key_2',2),('Key_3',3),('Key_4',4),('Key_5',5),('Key_6',6)]
        self.assertEqual(pq.heap,[])
        pq.insert(loi[5][0],loi[5][1])
        self.assertEqual(pq.heap,[(6,0,'Key_6')])
        pq.insert(loi[2][0],loi[2][1])
        self.assertEqual(pq.heap,[(3,1,'Key_3'),(6,0,'Key_6')])
        pq.insert(loi[4][0],loi[4][1])
        self.assertEqual(pq.heap,[(3,1,'Key_3'),(6,0,'Key_6'),(5,2,'Key_5')])
        pq.insert(loi[1][0],loi[1][1])
        self.assertEqual(pq.heap,[(2,3,'Key_2'),(3,1,'Key_3'),(5,2,'Key_5'),(6,0,'Key_6')])
        pq.insert(loi[3][0],loi[3][1])
        self.assertEqual(pq.heap,[(2,3,'Key_2'),(3,1,'Key_3'),(5,2,'Key_5'),(6,0,'Key_6'),(4,4,'Key_4')])
        pq.insert(loi[0][0],loi[0][1])
        self.assertEqual(pq.heap,[(1,5,'Key_1'),(3,1,'Key_3'),(2,3,'Key_2'),(6,0,'Key_6'),(4,4,'Key_4'),(5,2,'Key_5')])
    
    def test_get_min(self):
        pq = MinHeap()
        loi = [('Key_1',1),('Key_2',2),('Key_3',3),('Key_4',4),('Key_5',5),('Key_6',6)]
        for x in loi:
            pq.insert(x[0],x[1])
        self.assertEqual(pq.getMin(),'Key_1')
        self.assertEqual(pq.getMin(),'Key_2')
        self.assertEqual(pq.getMin(),'Key_3')
        self.assertEqual(pq.getMin(),'Key_4')
        self.assertEqual(pq.getMin(),'Key_5')
        self.assertEqual(pq.getMin(),'Key_6')

    def test_update(self):
        pq = MinHeap()
        loi = [('Key_1',2),('Key_2',4),('Key_3',6),('Key_4',8),('Key_5',10),('Key_6',12)]
        for x in loi:
            pq.insert(x[0],x[1])
        pq.update('Key_6',1)
        pq.update('Key_4',0)
        pq.update('Key_5',7)
        pq.update('Key_3',3)
        self.assertEqual(pq.getMin(),'Key_4')
        self.assertEqual(pq.getMin(),'Key_6')
        self.assertEqual(pq.getMin(),'Key_1')
        self.assertEqual(pq.getMin(),'Key_3')
        self.assertEqual(pq.getMin(),'Key_2')
        self.assertEqual(pq.getMin(),'Key_5')
    
    def test_is_empty(self):
        pq = MinHeap()
        self.assertTrue(pq.isEmpty())
        pq.insert('Key_1',2)
        self.assertFalse(pq.isEmpty())


if __name__ == '__main__':
    unittest.main()