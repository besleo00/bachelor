from Algorithmen.Dijkstra import dijkstra_algorithm
from Datenstruktur.Graph import Directed_Graph
from Datenstruktur.Node import Node
from Datenstruktur.Edge import Edge

import unittest


class TestDikstra(unittest.TestCase):
    def test_shortestpath(self):
        g = Directed_Graph()
        n1 = Node(0, 0)
        n2 = Node(1, 1)
        n3 = Node(2, 2)
        n4 = Node(3, 1)
        e1 = Edge(n1, n2)
        e2 = Edge(n1, n3)
        e3 = Edge(n2, n4)
        e4 = Edge(n3, n4)
        g.edge_lengths[(n1,n2)] = 2
        g.edge_lengths[(n1,n3)] = 2
        g.edge_lengths[(n2,n4)] = 1
        g.edge_lengths[(n3,n4)] = 2
        n1.adjacent.append(n2)
        n1.adjacent.append(n3)
        n2.adjacent.append(n4)
        n3.adjacent.append(n4)
        g.build_graph([n1, n2, n3, n4], [e1, e2, e3, e4])
        self.assertEqual(dijkstra_algorithm(g,n1,n4),3)

if __name__ == "__main__":
    unittest.main()
